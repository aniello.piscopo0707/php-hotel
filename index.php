<!DOCTYPE html>
<html lang="ita">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP Hotel</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
</head>

<?php

$hotels = [

    [
        'name' => 'Hotel Belvedere',
        'description' => 'Hotel Belvedere Descrizione',
        'parking' => true,
        'vote' => 4,
        'distance_to_center' => 10.4
    ],
    [
        'name' => 'Hotel Futuro',
        'description' => 'Hotel Futuro Descrizione',
        'parking' => true,
        'vote' => 2,
        'distance_to_center' => 2
    ],
    [
        'name' => 'Hotel Rivamare',
        'description' => 'Hotel Rivamare Descrizione',
        'parking' => false,
        'vote' => 1,
        'distance_to_center' => 1
    ],
    [
        'name' => 'Hotel Bellavista',
        'description' => 'Hotel Bellavista Descrizione',
        'parking' => false,
        'vote' => 5,
        'distance_to_center' => 5.5
    ],
    [
        'name' => 'Hotel Milano',
        'description' => 'Hotel Milano Descrizione',
        'parking' => true,
        'vote' => 2,
        'distance_to_center' => 50
    ],

];

$vote = (isset($_GET['vote'])) ? $_GET['vote'] : 1;

if (isset($_GET['parkingAvailability'])) {
    $parkingAvailability = $_GET['parkingAvailability'];

    $parkingAvailability = ($parkingAvailability == 'true') ? true : false;

    $hotelsWithParkingArea = array_filter($hotels, function ($hotel) use ($parkingAvailability) {
        if ($hotel['parking'] === $parkingAvailability) {
            return $hotel;
        }
    });
} else {
    $hotelsWithParkingArea = $hotels;
}

$hotelsFilteredByVote = array_filter($hotels, function ($hotel) use ($vote) {
    if ($hotel['vote'] >= $vote) {
        return $hotel;
    }
});

$hotelsFiltered = array_intersect_key($hotelsFilteredByVote, $hotelsWithParkingArea);
?>



<body>
    <form action="./index.php" method="GET" class="input-group">
        <input type="radio" name="parkingAvailability" value="true">
        <label for="parkingAvailable">Parcheggio</label>
        <input type="radio" name="parkingAvailability" value="false">
        <label for="parkingAvailable">Senza parcheggio</label>
        <input type="number" name="vote" placeholder="Inserisci voto da 1 a 5" min='1' max='5'>
        <button type="submit" class="btn btn-primary">Ricerca</button>
    </form>

    <main>
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th scope="col">LISTA HOTEL</th>
                    <th scope="col">Descrizione</th>
                    <th scope="col">Disponibilità parcheggio</th>
                    <th scope="col">Voto</th>
                    <th scope="col">Distanza dal centro</th>
                </tr>
            </thead>
            <tbody>
                <?php

                foreach ($hotelsFiltered as $hotel) {
                    $parkingAvailabilityString = ($hotel['parking'] ? '&#x2713;' : '&#x2717;');
                    echo "
                        <tr>
                            <th scope='row'>{$hotel['name']}</th>
                            <td>{$hotel['description']}</td>
                            <td>{$parkingAvailabilityString}</td>
                            <td>{$hotel['vote']}</td>
                            <td>{$hotel['distance_to_center']}m</td>
                        </tr>
                    ";
                }
                ?>
            </tbody>
        </table>

    </main>
</body>

</html>